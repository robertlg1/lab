import Foundation

class Flashcardset{
    let title: String
    
    init() {
        let title1 = FlashcardSet("Lakers")
        let title2 = FlashcardSet("Bucks")
        let title3 = FlashcardSet("Warriors")
        let title4 = FlashcardSet("Dark Knight")
        let title5 = FlashcardSet( "Man of Steel")
        let title6 = FlashcardSet("Emerald Warrior")
        let title7 = FlashcardSet("Super Soldier")
        let title8 = FlashcardSet("Spider")
        let title9 = FlashcardSet("Best there is at what he does")
        let title10 = FlashcardSet("Scarlet Speedster")

        let fc = mutableListOf(true,false)
        fc.add(title1)
        fc.add(title2)
        fc.add(title3)
        fc.add(title4)
        fc.add(title5)
        fc.add(title6)
        fc.add(title7)
        fc.add(title8)
        fc.add(title9)
        fc.add(title10)

        return fc
    }
    
}